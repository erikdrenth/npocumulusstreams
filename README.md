# NPO Streams voor Kodi Live TV#

Python script dat werkende NPO streams ophaalt en combineert XMLTV-compatible EPG. Genereert een M3U die te gebruiken is in Kodi.
Resultaat: NPO streams in Kodi Live TV.

* Gebaseerd op [Nederland 24 van Bosel](https://github.com/bosel/plugin.video.nederland24)
* Voor EPG gebruik tv_grab_nl3.py uit [tvgrabpyAPI](https://github.com/tvgrabbers/tvgrabpyAPI)
* Werkt samen met [PVR IPTV Simple](https://github.com/kodi-pvr/pvr.iptvsimple), een plugin voor [Kodi](https://kodi.tv)

Dit script kan draaien op de zelfde computer waar ook Kodi op ge�nstalleerd staat, maar dat is niet noodzakelijk. Door streams.py met een cronjob aan te roepen kun je een m3u maken in een via http bereikbare (lokale) netwerklocatie, en die in je hele netwerk door Kodi aan laten spreken.