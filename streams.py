#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import urllib
import urllib2
import re
import urlparse
import httplib
import cgi, cgitb
import subprocess
try:
   import cPickle as pickle
except:
   import pickle


API_URL = 'http://ida.omroep.nl/app.php/'
BASE_URL = 'http://livestreams.omroep.nl/live/npo/'
#USER_AGENT = 'Mozilla/5.0 (iPad; CPU OS 8_1 like Mac OS X) AppleWebKit/600.1.4 (KHTML, like Gecko) Version/8.0 Mobile/12B410 Safari/600.1.4'
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_4) AppleWebKit/600.7.12 (KHTML, like Gecko) Version/8.0.7 Safari/600.7.12'
REF_URL = 'http://www.npo.nl'
TOKEN_URL = 'http://ida.omroep.nl/app.php/auth'

M3U_DIR = '/var/www/html/streams/' #same as above
IP = 'http://192.168.1.4'
STREAM_BASE = IP + '/cgi-bin/npocumulusstreams/streams.py?id='

CHANNELS = [
  
  ["NPO 1", "npo_1.png", "LI_NL1_4188102", "Televisiekijken begint op NPO 1. Van nieuws en actualiteiten tot consumentenprogramma's en kwaliteitsdrama. Programma's die over jou en jouw wereld gaan. Met verhalen die je herkent over mensen die zomaar in je straat kunnen wonen. Ook als er iets belangrijks gebeurt, in Nederland of in de wereld, kijk je NPO 1."],
  ["NPO 2", "npo_2.png", "LI_NL2_4188105", "NPO 2 zet je aan het denken. Met programma's die verdiepen en inspireren. Als je wilt weten wat het verhaal achter de actualiteit is. Of als je het eens van een andere kant wilt bekijken. NPO 2 biedt het mooiste van Nederlandse en internationale kunst en cultuur, literatuur, documentaires, art-house films en kwaliteitsdrama."],
  ["NPO 3", "npo_3.png", "LI_NL3_4188107", "Op NPO 3 vind je programma's waar jong Nederland zich in herkent en die je uitdagen een eigen mening te vormen. Met veel aandacht voor nieuwe media en experimentele vernieuwing brengt NPO 3 een gevarieerd aanbod van de dagelijkse actualiteit tot muziek, reizen, human interest, talkshows en documentaires."],
  ["RTV Noord", "rtvnoord.png", "http://livestreams.omroep.nl/live/npo/regionaal/rtvnoord/rtvnoord.isml/rtvnoord.m3u8?protection=url", ""],
  ["NPO 101", "npo_101.png", "LI_NEDERLAND3_221683", "Weg met suffe en saaie tv! Het is tijd voor NPO 101, het 24-uurs jongerenkanaal van BNN en de Publieke Omroep. Met rauwe en brutale programma's, van en voor jongeren. Boordevol hilarische fragmenten, spannende livegames, bizarre experimenten en nieuws over festivals en gratis concertkaartjes. Kijken dus!"],
#  ["NPO Best", "npo_best.png", "thematv/best24/best24.isml/best24.m3u8", "NPO Best brengt hoogtepunten uit ruim zestig jaar Nederlandse televisiehistorie. Het is een feelgoodzender waarop u 24 uur per dag de mooiste programma's uit de schatkamer van de Publieke Omroep kunt zien."],
  ["NPO Cultura", "npo_cultura.png", "LI_NEDERLAND2_221679", "NPO Cultura is het digitale themakanaal van de Publieke Omroep voor verdieping in kunst en cultuur. 24 uur per dag programma's uit genres als klassiek, literatuur, dans, theater, pop, jazz, film, drama en beeldende kunst."],
  ["NPO Zapp Xtra / Best", "npo_zapp.png", "LI_NEDERLAND3_221687", "Zappelin Xtra en Zapp Xtra zendt dagelijks, 24 uur per dag en reclamevrij, de beste kinderprogramma's van de publieke omroep uit. Aansluitend op Nederland 3 zendt het themakanaal programma's uit van Zappelin of Zapp. Is op Nederland 3 iets voor kleuters te zien dan richt het themakanaal zich op oudere kinderen, en andersom."],
#  ["NPO Doc", "npo_doc.png", "thematv/hollanddoc24/hollanddoc24.isml/hollanddoc24.m3u8", "NPO Doc is het documentaireplatform van de NPO en verrast je continu met de beste documentaires uit binnen- en buitenland en exclusieve interviews met regisseurs en topwetenschappers."],
#  ["NPO Humor TV", "npo_humor_tv.png", "thematv/humor24/humor24.isml/humor24.m3u8", "NPO Humor TV is een uitgesproken comedykanaal: een frisse, Nederlandse humorzender met hoogwaardige, grappige, scherpe, jonge, nieuwe, satirische, humoristische programma's."],
  ["NPO Nieuws", "npo_nieuws.png", "LI_NEDERLAND1_221673", "Via het themakanaal 'NPO Nieuws' kunnen de live televisieuitzendingen van het NOS Journaal worden gevolgd. De laatste Journaaluitzending wordt herhaald tot de volgende uitzending van het NOS Journaal."],
  ["NPO Politiek", "npo_politiek.png", "LI_NEDERLAND1_221675", "NPO Politiek is het digitale kanaal over de Nederlandse politiek in de breedste zin van het woord."],
  ["NPO Radio 1", "npo_radio1.png", "LI_RADIO1_300877", "De onafhankelijke nieuws- en sportzender. Als er iets belangrijks gebeurt, in Nederland of in de wereld, luister je NPO Radio 1. Voor de achtergronden en het nieuws van alle kanten. Ook jouw mening telt. Er is veel ruimte voor opinie en debat waar ook luisteraars steevast aan deelnemen."],
  ["NPO Radio 2", "npo_radio2.png", "LI_RADIO2_300879", "Informatie, actualiteit en het beste uit vijftig jaar popmuziek. Een toegankelijke zender met veel aandacht voor het Nederlandse lied, kleinkunst en cabaret."],
  ["NPO 3FM", "npo_3fm.png", "LI_3FM_300881", "Op NPO 3FM staat de liefde voor muziek centraal. Samen met de luisteraar vindt NPO 3FM nieuwe muziek, nieuw Nederlands poptalent en jong radiotalent. Je komt onze dj's vaak tegen op festivals en concerten."],
  ["NPO Radio 4", "npo_radio4.png", "LI_RA4_698901", "De klassieke muziekzender voor zowel de ervaren als de nieuwe liefhebber. Naast de mooiste klassieke muziek, brengt NPO Radio 4 jaarlijks ongeveer twaalfhonderd concerten uit 's werelds beroemdste concertzalen. Waaronder drie eigen concertseries."],
  # ["NPO Radio 5", "npo_radio5.png", "tvlive/mcr1/mcr1.isml/mcr1.m3u8", "Overdag is NPO Radio 5 een sfeervolle zender met vooral evergreens uit de jaren ’60 en ’70. In de avond en het weekeinde brengt NPO Radio 5 beschouwende en informatieve programma’s over: godsdienst, levensbeschouwing en specifieke (sub)culturen."],
  # ["NPO Radio 6", "npo_radio6.png", "visualradio/radio6/radio6.isml/radio6.m3u8", "De Soul & Jazz zender, met muziek van Miles Davis tot Caro Emerald. Onze dj’s en muzikanten nemen je mee op een nationale en internationale ontdekkingstocht. Daarnaast doet NPO Radio 6 jaarlijks verslag van festivals als North Sea Jazz."],
  ["NPO FunX", "npo_funx.png", "LI_3FM_603983", "FunX richt zich op alle jongeren tussen 15 en 35 jaar. Muziekstijlen die op FunX te horen zijn zijn urban, latin, reggae en dancehall, oriental, Turkpop, farsipop, banghra, rai, Frans-Afrikaanse hiphop, Mandopop en andere crossoverstijlen."],
  ["Radio Noord", "rtvnoord.png", "http://livestreams.omroep.nl/live/npo/regionaal/rtvnoord2/rtvnoord2.isml/rtvnoord2.m3u8?protection=url", ""],
#  ["Sky News", "skynews.png", "YT_y60wDzZt8yg", ""],
#  ["Bloomberg TV", "bloomberg.png", "YT_Ga3maNZ0x0w", ""],
#  ["Al Jazeera", "aljazeera.png", "YT_dp6W0ZcYwE4", ""],
]





XMLID = {
	"NPO 1": "0-1",
	"NPO 2": "0-2",
	"NPO 3": "0-3",
	"NPO 101": "0-410",
#	"NPO 1", "0-1"],
	"NPO Cultura": "0-70",
	"NPO Zapp Xtra / Best": "1-zappelin",
#	"NPO 1", "0-1"],
#	"NPO 1", "0-1"],
	"NPO Nieuws": "1-journaal-24",
	"NPO Politiek": "1-politiek-24",
	"NPO Radio 1": "4-292",
	"NPO Radio 2": "4-293",
	"NPO 3FM": "4-294",
	"NPO Radio 4": "4-295",
#	["NPO 3FM", "4-294"],
#	["NPO 3FM", "4-294"],
	"NPO FunX": "4-298",
	"RTV Noord": "0-108",
	"Radio Noord": "7-radio_noord",
#	"Sky News": "",
#	"Bloomberg TV": "",
#	"Al Jazeera": "0-423",
}


def resolve_http_redirect(url, depth=0):
    if depth > 10:
        raise Exception("Redirected "+depth+" times, giving up.")
    o = urlparse.urlparse(url, allow_fragments=True)
    conn = httplib.HTTPConnection(o.netloc)
    path = o.path
    if o.query:
        path += '?'+o.query
    conn.request("HEAD", path)
    res = conn.getresponse()
    headers = dict(res.getheaders())
    if headers.has_key('location') and headers['location'] != url:
        return resolve_http_redirect(headers['location'], depth+1)
    else:
        return url

def collect_token():
    req = urllib2.Request(TOKEN_URL)
    req.add_header('User-Agent', USER_AGENT)
    response = urllib2.urlopen(req)
    page = response.read()
    response.close()
    token = re.search(r'token\":"(.*?)"', page).group(1)
    #xbmc.log("plugin.video.nederland24:: token: %s" % token)
    return token


def playVideo(url):
    media = url
    finalUrl = ""
    if media and media.startswith("http://") or media.startswith("https://"):
        finalUrl = media
    elif media and media.startswith("YT_"):
	media = (media.replace('YT_', ''))
        finalUrl = youtube(media)
    else:
        URL = API_URL+media+"?adaptive=yes&token=%s" % collect_token()
        #xbmc.log("plugin.video.nederland24:: URL and token %s" % str(URL))
        req = urllib2.Request(URL)
        req.add_header('User-Agent', USER_AGENT)
        req.add_header('Referer', REF_URL)
        req.add_header('HTTP_NPOPLAYER_VERSION', '7.2.4')
        response = urllib2.urlopen(req)
        page = response.read()
        #xbmc.log("plugin.video.nederland24:: intermediate URL %s" % str(page))
        response.close()
        videopre = re.search(r'(http.*?)\"', page).group(1)
        prostream = (videopre.replace('\/', '/'))
        intermediateURL=resolve_http_redirect(prostream)
        req = urllib2.Request(intermediateURL)
        req.add_header('User-Agent', USER_AGENT)
        req.add_header('Referer', REF_URL)
        req.add_header('HTTP_NPOPLAYER_VERSION', '7.2.4')
        response = urllib2.urlopen(req)
        page = response.read()
        response.close()
        videopre = re.search(r'(http.*?)\"', page).group(1)
        prostream = (videopre.replace('\/', '/'))
        #xbmc.log("plugin.video.nederland24:: final URL %s" % str(prostream))
        #no longer required
        #finalUrl = resolve_http_redirect(prostream)
        #TODO: this is a workaround for unplayable https urls (might be due to some sort of kodi bug on linux, on osx these play fine)
        finalUrl = (prostream.replace('https', 'http'))
    return finalUrl

def getstream(id):
    return playVideo(CHANNELS[id][2])


def youtube(setting = 0, value = 0):
  try:
   settings = pickle.load( open( "youtube.p", "rb" ) )
  except:
   settings = {}

  if setting is 0 and value is 0:
   return settings
  elif value is 0:
   return settings[setting]
  else:
   settings[setting] = value
   pickle.dump( settings, open( "youtube.p", "wb" ) )
   return settings



cgitb.enable()

form = cgi.FieldStorage()

if form.has_key("id"):
 print ("Location: "+getstream(int(form["id"].value)))
 print # to end the CGI response headers.
 quit()

m3u = "#EXTM3U\n"

for id,chan in enumerate(CHANNELS):
 m3u = m3u + "#EXTINF:-1 tvg-id=\"" + XMLID[chan[0]] + "\" tvg-name=\"" + chan[0] + "\" tvg-logo=\"logos/" + chan[1] + "\"," + chan[0] + "\n"
 m3u = m3u + STREAM_BASE + str(id) + "\n\n"
 fm = open (M3U_DIR + "s.m3u","w")
 fm.write(m3u)
 fm.close()
# if chan[2].startswith("YT_"):
# 	m = (chan[2].replace('YT_', ''))
#        url = subprocess.check_output("youtube-dl -g https://www.youtube.com/watch?v="+m, shell=True)
#	youtube(m,url)

